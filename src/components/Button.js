import React from 'react';

const Button = ({handleOnClick,color,text}) => {
   
        return ( 
            <button onClick ={handleOnClick} className={color}>{text}</button>
           
         );
   
}
 
export default Button;



// Class

// import React, { Component } from 'react';

// class Button extends Component {
   
//     // Props or Property for class Based
//     render() { 
//         return ( 
//             <button onClick ={this.props.handleOnClick} className={this.props.color}>{this.props.text}</button>
           
//          );
//     }
// }
 
// export default Button;