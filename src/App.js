import React, {useState} from 'react';
// import box Component
import Box from './components/Box'
// import button Component
import Buttons from './components/Buttons'

const App = () => {
  let [count, setCount] = useState (0)

  const handleAdd = () => {
    setCount (count + 1)
  }

  const handleMinus = () => {
    setCount (count - 1)
  }

  const handleReset = () => {
    setCount (count = 0 )
  }

  const handleTimesTwo = () => {
    setCount (count * 2 )
  }


    return ( 
      <>
        <Box
          count = { count }
        />
        <Buttons
          handleAdd = {handleAdd}
          handleMinus = {handleMinus}
          handleReset = { handleReset }
          handleTimesTwo = {handleTimesTwo}
        />
        
      </>
     );
  }

export default App;




// import React, {Component} from 'react';

// // import box Component
// import Box from './components/Box'
// // import button Component
// import Buttons from './components/Buttons'

// class App extends Component {
//   state ={
//     count:0
//   }

//   handleAdd = () => {
//     this.setState({
//       count : this.state.count + 1
//     })
//   }

//   handleMinus = () => {
//     this.setState({
//       count : this.state.count - 1
//     })
//   }

//   handleReset = () => {
//     this.setState({
//       count:0
//     })
//   }

//   handleTimesTwo = () => {
//     this.setState({
//       count : this.state.count * 2
//     })
//   }

//   render() { 
//     return ( 
//       <>
//         <Box
//           count = { this.state.count }
//         />
//         <Buttons
//           handleAdd = {this.handleAdd}
//           handleMinus = {this.handleMinus}
//           handleReset = { this.handleReset }
//           handleTimesTwo = {this.handleTimesTwo}
//         />
        
//       </>
//      );
//   }
// }
 
// export default App;